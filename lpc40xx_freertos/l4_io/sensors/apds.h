#pragma once

#include <delay.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define kMaxFIFOLevel 33
#define kMaxFifoSize 128
#define kMaxDataSize 8
#define APDS_DEVICE_ID 0xA8
#define ENABLE_GESTURE 0x4D // POWERON| kPROXIMITYDETECT|kWAIT |kGESTURE

typedef enum {
  kPOWERON = 0,
  kALSENABLE,
  kPROXIMITYDETECT,
  kWAIT,
  kALSINTERRUPT,
  kPROXIMITYINTERRUPT,
  kGESTURE
} mode_e;
typedef enum {
  kSwipeUP = 'U',
  kSwipeDOWN = 'D',
  kSwipeLEFT = 'L',
  kSwipeRIGHT = 'R',
  kNEAR = 'N',
  kFAR = 'F',
  kError = 'E'
} gesture_e;

typedef enum {
  kNone = 'x',
  kUp = 'u',
  kDown = 'd',
  kLeft = 'l',
  kRight = 'r',
  kFar = 'f',
  kNear = 'n'
} direction_e;

// Defualt values for Gesture sensor initialization
typedef enum {
  kNoMode = 0x00,
  kAtime = 0xDB,
  kWtime = 0xF6,
  kPilt = 0x00,
  kPiht = 0x32,
  kPers = 0x11,
  kConfig1 = 0x60,
  kPpulse = 0x89,
  kControl = 0x05,
  kConfig2 = 0x01,
  kPoffsetUpRightDownLeft = 0x00,
  kConfig3 = 0x00,
  kGPEnTh = 0x14,
  kGExTh = 0x64,
  kGconfig1 = 0x43,
  kGconfig2 = 0x66,
  kGoffsetUpDown = 0x00,
  kGoffsetLeftRight = 0x00,
  kGPulse = 0xC9,
  kGconfig3 = 0x00,
  kGconfig4 = 0x02,
  kAilt = 0xFF,
  kAiht = 0x00
} gesture_default_settings_e;

bool apds__init(void);
gesture_e GetGesture(void);
uint8_t apds__get_proximity(void);
