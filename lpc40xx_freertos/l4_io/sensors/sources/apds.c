#include "apds.h"

#include "i2c.h"
#include <math.h>

static const i2c_e apds__sensor_bus = I2C__2;

uint8_t gfifo_data[kMaxFifoSize];
direction_e points[kMaxDataSize];
uint8_t far_count_ = 0;
uint8_t near_count_ = 0;
uint8_t index_ = 0;
int8_t up_sensitivity_;
int8_t down_sensitivity_;
int8_t left_sensitivity_;
int8_t right_sensitivity_;
int16_t far_sensitivity_;
int16_t near_sensitivity_;
static const uint8_t APDS__SLAVE_ADDRESS = 0x73;
typedef enum {
  apds__address = 0x72,
  apds__enable_register = 0x80,
  apds__adc_integration_time = 0x81,
  apds__wait_time_register = 0x83,
  apds__low_threshold_lowbyte = 0x84,
  apds__low_threshold_highbyte = 0x85,
  apds__high_threshold_lowbyte = 0x86,
  apds__high_threshold_highbyte = 0x87,
  apds__proximity_int_low_threshold = 0x89,
  apds__proximity_int_high_threshold = 0x8B,
  apds__interrupt_persistence_filters_nongesture = 0x8C,
  apds__config_register_one = 0x8D,
  apds__proximity_pulse_count = 0x8E,
  apds__gain_control = 0x8f,
  apds__config_register_two = 0x90,
  apds__memory_who_am_i = 0x92,
  apds__proximity_offset_up_right = 0x9D,
  apds__proximity_offset_down_left = 0x9E,
  apds__config_register_three = 0x9F,
  apds__gesture_enter_threshold = 0xA0,
  apds__gesture_exit_threshold = 0xA1,
  apds__gesture_configuration_1 = 0xA2,
  apds__gesture_configuration_2 = 0xA3,
  apds__gesture_pulse_count_and_length = 0xA6,
  apds__gesture_left_offset = 0xA7,
  apds__gesture_right_offset = 0xA9,
  apds__gesture_configuration_3 = 0xAA,
  apds__gesture_configuration_4 = 0xAB,
  apds__gesture_fifo_level = 0xAE,
  apds__gesture_status_register = 0xAF,
  apds__gesture_fifo_data = 0xFC
} apds__memory_e;

bool FindDevice() {
  bool device_found = false;
  uint8_t device_id = i2c__read_single(apds__sensor_bus, APDS__SLAVE_ADDRESS,
                                       apds__memory_who_am_i);

  if (device_id == APDS_DEVICE_ID) {
    device_found = true;
  }
  return device_found;
}

void Initialize(void) {
  if (FindDevice()) {
    i2c__write_single(apds__sensor_bus, apds__address, apds__enable_register,
                      kNoMode);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__adc_integration_time, kAtime);
    i2c__write_single(apds__sensor_bus, apds__address, apds__wait_time_register,
                      kWtime);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__low_threshold_lowbyte, kAilt);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__low_threshold_highbyte, kAilt);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__high_threshold_lowbyte, kAiht);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__high_threshold_highbyte, kAiht);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__proximity_int_low_threshold, kPilt);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__proximity_int_high_threshold, kPiht);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__interrupt_persistence_filters_nongesture, kPers);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__config_register_one, kConfig1);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__proximity_pulse_count, kPpulse);
    i2c__write_single(apds__sensor_bus, apds__address, apds__gain_control,
                      kControl);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__config_register_two, kConfig2);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__proximity_offset_up_right, kPoffsetUpRightDownLeft);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__proximity_offset_down_left,
                      kPoffsetUpRightDownLeft);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__config_register_three, kConfig3);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__gesture_enter_threshold, kGPEnTh);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__gesture_exit_threshold, kGExTh);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__gesture_configuration_1, kGconfig1);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__gesture_configuration_2, kGconfig2);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__gesture_left_offset, kGoffsetLeftRight);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__gesture_right_offset, kGoffsetLeftRight);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__gesture_pulse_count_and_length, kGPulse);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__gesture_configuration_3, kGconfig3);
    i2c__write_single(apds__sensor_bus, apds__address,
                      apds__gesture_configuration_4, kGconfig4);
  } else {
    printf("Error Initializing Gesture Sensor\n");
  }
}

/*Tells the state machine to either enter or exit gesture state machine*/
bool setGestureMode(uint8_t mode) {
  uint8_t val;
  /* Read value from GCONF4 register */
  val = i2c__read_single(apds__sensor_bus, apds__address,
                         apds__gesture_configuration_4);

  /* Set bits in register to given value */
  mode &= 0b00000001;
  val &= 0b11111110;
  val |= mode;

  /* Write register value back into GCONF4 register */
  i2c__write_single(apds__sensor_bus, apds__address,
                    apds__gesture_configuration_4, val);
  return true;
}

bool setLEDBoost(uint8_t boost) {
  uint8_t val;
  /* Read value from CONFIG2 register */
  val = i2c__read_single(apds__sensor_bus, apds__address,
                         apds__config_register_two);

  /* Set bits in register to given value */
  boost &= 0b00000011;
  boost = boost << 4;
  val &= 0b11001111;
  val |= boost;

  i2c__write_single(apds__sensor_bus, apds__address, apds__config_register_two,
                    val);
  return true;
}

bool EnableGesture(void) {
  bool status = false;
  const uint8_t kShortWaitTime = 0xFF;
  const uint8_t kK16UsPulseLengthAnd10PulseCount = 0x89;
  //  const uint8_t kClearProximityLedInterruptChannelAndLedBoost150 = 0xD0;

  i2c__write_single(apds__sensor_bus, apds__address, apds__wait_time_register,
                    kShortWaitTime);
  i2c__write_single(apds__sensor_bus, apds__address,
                    apds__proximity_pulse_count,
                    kK16UsPulseLengthAnd10PulseCount);
  // i2c__write_single(apds__sensor_bus, apds__address,
  // apds__config_register_two,
  //                   kClearProximityLedInterruptChannelAndLedBoost150);
  setLEDBoost(3);
  setGestureMode(1);
  status = i2c__write_single(apds__sensor_bus, apds__address,
                             apds__enable_register, ENABLE_GESTURE);
  return status;
}

bool apds__init(void) {
  bool status = false;
  if (FindDevice()) {
    printf("Found it!\n");
    printf("Initializing Gesture Sensor...\n");
    Initialize();
    status = EnableGesture();
  } else {
    printf("Gesture sensor not found!\n");
  }

  return status;
}

bool CheckIfGestureOccured(void) {
  uint8_t gesture_status = i2c__read_single(apds__sensor_bus, apds__address,
                                            apds__gesture_status_register);
  return (gesture_status & 0b00000001);
}

bool ReadGestureMode(void) {
  uint8_t value_msb = i2c__read_single(apds__sensor_bus, apds__address,
                                       apds__gesture_configuration_3);
  uint8_t value_lsb = i2c__read_single(apds__sensor_bus, apds__address,
                                       apds__gesture_configuration_4);
  uint16_t value = (value_msb << 8) | (value_lsb);
  return (value & 0x0100);
}

uint8_t GetGestureFIFOLevel(void) {
  const uint8_t kOverflowFlagPosition = 1;
  uint8_t value = 0;
  uint8_t gesture_status = i2c__read_single(apds__sensor_bus, apds__address,
                                            apds__gesture_status_register);
  if (gesture_status &
      (1 << kOverflowFlagPosition)) // if overflow, clear FIFO data
  {
    printf("Overflow. No Gesture Detected\n");
    value = kMaxFIFOLevel; // Overflow happens at FIFO level 33
  } else {
    value = i2c__read_single(apds__sensor_bus, apds__address,
                             apds__gesture_fifo_level);
  }
  return value;
}

void ReadGestureFIFO(uint8_t *data, uint8_t fifolevel) {
  int amount_of_data_to_read = (4 * fifolevel);
  i2c__read_slave_data(apds__sensor_bus, apds__address, apds__gesture_fifo_data,
                       data, amount_of_data_to_read);
}

void ProcessGestureData(uint8_t level) {
  const uint8_t kMaxPointsIndex = 7;
  // Read Gesture FIFOs (U/D/L/R)
  // Store raw gesture data in a 4x32 uint8_t array
  for (int i = 0; i < (4 * level); i += 4) {
    if (index_ > kMaxPointsIndex) {
      index_ = 0;
    }

    // printf("Fifo-%d[%i]:%d,%d,%d,%d\n", i, index_, gfifo_data[i],
    // gfifo_data[i + 1], gfifo_data[i + 2],gfifo_data[i + 3]); Check which
    // photodiode pair sensed more IR light
    if (abs((gfifo_data[i] - gfifo_data[i + 1])) >
        abs((gfifo_data[i + 2] - gfifo_data[i + 3]))) {
      // If a photodiode has a smaller value,
      // then that photodiode was covered and movement detected
      if ((gfifo_data[i] - gfifo_data[i + 1]) < up_sensitivity_) // Up
      {
        points[index_] = kUp;
        index_++;
      } else if ((gfifo_data[i] - gfifo_data[i + 1]) >
                 down_sensitivity_) // Down
      {
        points[index_] = kDown;
        index_++;
      }
    } else if (abs((gfifo_data[i] - gfifo_data[i + 1])) <
               abs((gfifo_data[i + 2] - gfifo_data[i + 3]))) {
      // If a photodiode has a smaller value,
      // then that photodiode was covered and movement detected
      if ((gfifo_data[i + 2] - gfifo_data[i + 3]) < left_sensitivity_) // Left
      {
        points[index_] = kLeft;
        index_++;
      } else if ((gfifo_data[i + 2] - gfifo_data[i + 3]) >
                 right_sensitivity_) // Right
      {
        points[index_] = kRight;
        index_++;
      }
    }

    // Check the sum of all photodiodes
    if ((gfifo_data[i] + gfifo_data[i + 1] + gfifo_data[i + 2] +
         gfifo_data[i + 3]) > near_sensitivity_) // Near
    {
      near_count_++;
    } else if ((gfifo_data[i] + gfifo_data[i + 1] + gfifo_data[i + 2] +
                gfifo_data[i + 3]) < far_sensitivity_) // Far
    {
      far_count_++;
    }
  }
}

gesture_e DecodeGestureData(uint8_t level) {
  gesture_e result = kError;
  const uint8_t kFarNearSensitivity = 15;
  const uint8_t kNearFarCountMinimum = 5;

  if (level == kMaxFIFOLevel) {
    result = kError;
  } else if (level > kFarNearSensitivity) {
    if (far_count_ > kNearFarCountMinimum) {
      result = kFAR;
    } else if (near_count_ > kNearFarCountMinimum) {
      result = kNEAR;
    }
  }

  //  delay__ms(30);
  for (int i = 0; i < kMaxDataSize - 1; i++) {
    // Check initial point, then check next point
    // to figure out the direction of the swipe
    switch (points[i]) {
    case kUp:
      if (points[i + 1] == kDown && result == kError) {
        result = kSwipeDOWN;
      }
      break;
    case kDown:
      if (points[i + 1] == kUp && result == kError) {
        result = kSwipeUP;
      } else if (points[i + 1] == kLeft && result == kError) {
        result = kSwipeLEFT;
      } else if (points[i + 1] == kRight && result == kError) {
        result = kSwipeRIGHT;
      }
      break;
    case kLeft:
      if (points[i + 1] == kRight && result == kError) {
        result = kSwipeRIGHT;
      } else if (points[i + 1] == kDown && result == kError) {
        result = kSwipeRIGHT;
      }
      break;
    case kRight:
      if (points[i + 1] == kLeft && result == kError) {
        result = kSwipeLEFT;
      } else if (points[i + 1] == kDown && result == kError) {
        result = kSwipeLEFT;
      }
      break;
      // If no valid point, then check near/far counts
    default:
      break;
    }
    // printf("%i:[%c - %c] - [%c]\n", i, points[i], points[i + 1], result);
  }
  return result;
}

gesture_e GetGesture(void) {
  gesture_e result = kError;
  if (CheckIfGestureOccured()) {
    // check if gesture ended
    if (ReadGestureMode() == 0) {
      uint8_t level = 0;
      level = GetGestureFIFOLevel();
      ReadGestureFIFO(gfifo_data, level);
      // reset processing variables
      index_ = 0;
      near_count_ = 0;
      far_count_ = 0;
      for (int i = 0; i < 8; i++) {
        points[i] = kNone;
      }

      ProcessGestureData(level);
      result = DecodeGestureData(level);
    }
  }
  return result;
}